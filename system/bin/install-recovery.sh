#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:21136718:be132a908c2892c2b7dd1e73df21a4dc7067ba14; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:19240266:d979cdcc9228af189bd16d0a39b5402a69836c1c EMMC:/dev/block/bootdevice/by-name/recovery be132a908c2892c2b7dd1e73df21a4dc7067ba14 21136718 d979cdcc9228af189bd16d0a39b5402a69836c1c:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
